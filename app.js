var editor_textarea = document.getElementById('main-editor');
var editor = CodeMirror.fromTextArea(editor_textarea, {
    mode: "text/x-markdown",
    vimMode: true,
    keyMap: 'vim',
    matchBrackets: true,
    showCursorWhenSelecting: true,
    indentUnit: 4,
    theme: 'base16-light',
    indentWithTabs: false,
    scrollbarStyle: "overlay"
});
var commandDisplay = document.getElementById('command-display');
var keys = '';

CodeMirror.on(editor, 'update', function(instance) {
    // TODO: pipe events through RxJS?
});

CodeMirror.on(editor, 'vim-keypress', function(key) {
    keys = keys + key;
    commandDisplay.innerHTML = keys;
});

CodeMirror.on(editor, 'vim-command-done', function(e) {
    keys = '';
    commandDisplay.innerHTML = keys;
});